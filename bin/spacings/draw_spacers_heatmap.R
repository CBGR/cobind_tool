#####################
## Load R packages ##
#####################

REQUIRED_LIBRARIES <- c(
  "data.table",
  "dplyr",
  "ggplot2",
  "optparse",
  "png",
  "stringr")

for (lib in REQUIRED_LIBRARIES) {
  suppressPackageStartupMessages(library(
    lib, 
    character.only = TRUE, 
    quietly = TRUE))
}

####################
## Read arguments ##
####################

option_list = list(
  
  make_option(c("-i", "--spacing_folder"), type = "character", default = NULL,
              help = "Path to the folder with the spacing information and the logos. (Mandatory) ", metavar = "character"),
  
  make_option(c("-t", "--analysis_title"), type = "character", default = NULL,
              help = "Analysis title matching the title in the config.yaml. (Mandatory) ", metavar = "character"),
  
  make_option(c("-f", "--family_name"), type = "character", default = NULL,
              help = "Family or class name. (Mandatory) ", metavar = "character")
  
);
message("; Reading arguments from command line")
opt_parser = OptionParser(option_list = option_list)
opt = parse_args(opt_parser)

############
## Syntax ##
############

### Syntax:
## Rscript bin/main/spacings/draw_spacers_heatmap_extract_regions.R
##        -i example_analysis_0214/results/clustering_results/CTCF/spacings
##        -t example_analysis_0214
##        -f CTCF

########################
## Set variable names ##
########################

SPACING_FOLDER <- opt$spacing_folder
ANALYSIS_TITLE <- opt$analysis_title
FAMILY_NAME    <- opt$family_name

## Debug
# SPACING_FOLDER <- "example_analysis_0214/results/clustering_results/CTCF/spacings"
# ANALYSIS_TITLE <- "example_analysis_0214"
# FAMILY_NAME    <- "CTCF"

########################
## Read spacing table ##
########################

message("; Reading table with spacing information for ", FAMILY_NAME)
spacing_tab <- data.table::fread(file.path(SPACING_FOLDER, "spacings.txt"), header = FALSE)

##############################
## Finding flank extensions ##
##############################

message("; Finding flank extension information")
WORKING_FOLDER <- file.path(ANALYSIS_TITLE, "results")

## Finding flank sizes from existing data:
fasta_files <- list.files(file.path(WORKING_FOLDER, "datasets", (list.files(file.path(WORKING_FOLDER, "datasets"))[1]), "fasta"))

fasta_files <- grep(pattern = ".+subtd", fasta_files, value = TRUE)

right_flank_extension <- grep(pattern = ".+\\.0l\\d+r", fasta_files, value = TRUE)
left_flank_extension <- grep(pattern = ".+\\.\\d+l0r\\.", fasta_files, value = TRUE)

right_flank_extension <- gsub(pattern = ".+\\.bed\\.oriented", replacement = "", right_flank_extension)
left_flank_extension <- gsub(pattern = ".+\\.bed\\.oriented", replacement = "", left_flank_extension)

#####################################
## Constructing new spacings table ##
#####################################

## Extracting relevant info to build tables for left and right flanks.
message("; Creating a table with relevant info.")

spacing_tab <- spacing_tab[,c(1, 2, 11, 14, 15, 16)] %>%
  dplyr::rename(Core = V1,
                Cobinder = V2,
                Flank = V11,
                Spacer_length = V14,
                Location = V15,
                Subcluster = V16) %>%
  mutate(Dataset = gsub(Core, pattern = "_\\D{4,5}_\\d+_components_.+", replacement = "")) %>%
  mutate(Nb_components = stringr::str_extract(Core, pattern = "\\d+_components")) %>%
  mutate(Component = stringr::str_extract(Core, pattern = "Comp_\\d+")) %>%
  mutate(Component = gsub(Component, pattern = "Comp_", replacement = "")) %>%
  mutate(Path_to_indices = file.path(WORKING_FOLDER, "nmf_motifs", paste0(Dataset, "_", Flank), Nb_components, paste0("indices_", Component, ".txt")))

## Adding paths to flanks files:
spacing_tab$Path_to_flanks <- ifelse(
  spacing_tab$Flank == "right",
  yes = file.path(WORKING_FOLDER, "datasets", spacing_tab$Dataset, "fasta", paste0(spacing_tab$Dataset, ".bed.oriented", right_flank_extension)),
  no  = file.path(WORKING_FOLDER, "datasets", spacing_tab$Dataset, "fasta", paste0(spacing_tab$Dataset, ".bed.oriented", left_flank_extension))
)

## Combining left and right tables.
spacings_file_tab <- spacing_tab %>%
  arrange(Subcluster, Spacer_length, Dataset, Nb_components, Component) %>%
  select(Dataset, Flank, Path_to_flanks, Nb_components, Component, Path_to_indices, Cobinder, Spacer_length, Location, Subcluster)

## Counting unique regions for each subcluster/spacer case.
## Grouping the table and getting indices.
spacings_tab_grouped <- spacings_file_tab %>%
  group_by(Subcluster, Location, Spacer_length)

group_indices <- group_rows(spacings_tab_grouped)

## Extracting regions and counting them for each group.
nb_regions_in_group <- c()
cobinder_flanks <- c()
all_flanks <- c()
nb_regions_per_case <- c()

for (i in seq_along(group_indices)) {

  group_entry <- group_indices[[i]]
  
  group_flanks <- c()
  all_group_flanks <- c()
  
  for (j in seq_along(group_entry)) {

    entry_idx <- group_entry[j]
    entry <- spacings_file_tab[entry_idx, ]
    
    case_code <- paste0(entry$Dataset,
                        "_flank_", entry$Flank, "_",
                        entry$Nb_components,
                        "_Comp_", entry$Component,
                        "_subcluster_", entry$Subcluster,
                        "_spacer_", entry$Spacer_length, "_", entry$Location)
    
    # Reading indices.
    indices <- data.table::fread(file.path(entry$Path_to_indices))
    indices <- indices$V1
    
    ## Filtering
    flanks_seqs <- readLines(file.path(entry$Path_to_flanks))
    flanks_seqs <- grep(pattern = ">", flanks_seqs, value = TRUE)
    filtered_flanks <- flanks_seqs[indices + 1]
    
    group_flanks <- c(group_flanks, filtered_flanks)
    all_group_flanks <- c(all_group_flanks, flanks_seqs)
    
    ## Counting how many regions in filtered and unfiltered bed:
    nb_regions_original_flanks <- length(flanks_seqs)
    nb_regions_filtered_flanks <- length(filtered_flanks)
    nb_regions_one_case <- cbind(entry, case_code, nb_regions_original_flanks, nb_regions_filtered_flanks)
    nb_regions_per_case <- rbind(nb_regions_per_case, nb_regions_one_case)
    
  }

  group_length <- length(group_flanks)

  all_group_length <- length(all_group_flanks)
  
  ## Writing the numbers of regions in each group (with cobinder and without)
  nb_merged_regions <- cbind(all_group_length, group_length)
  nb_regions_in_group <- rbind(nb_regions_in_group, nb_merged_regions)
  
  ## Pooling cobinder and all beds:
  cobinder_flanks <- c(cobinder_flanks, group_flanks)
  all_flanks <- c(all_flanks, all_group_flanks)
  
}

###############################################################
## Adding the number of regions to the original spacings tab ##
###############################################################
  
spacings_tab_nb_regions <- merge(spacing_tab, nb_regions_per_case) %>%
  mutate(Subcluster = as.character(paste0("Co-binding ", as.character(Subcluster)))) %>%
  select(
    Cobinder, Core, Flank, Spacer_length, Location, Subcluster,
    Dataset, Nb_components, Component,
    nb_regions_original_flanks, nb_regions_filtered_flanks) %>%
  mutate(Fraction_of_regions_with_cobinder = as.numeric(nb_regions_filtered_flanks)/as.numeric(nb_regions_original_flanks))

colnames(spacings_tab_nb_regions) <- c(
  "Cobinder", "Anchor", "Flank", "Spacer_length", "Location", "Subcluster",
  "Dataset", "Nb_components", "Component",
  "Nb_regions_in_input_flanks", "Nb_regions_with_cobinder", "Fraction_of_regions_with_cobinder")

###########################################################
#########              SPACINGS PLOT              #########
###########################################################

############################################
## Editing spacings table for the heatmap ##
############################################

message("; Preparing data for plotting")

spacing_tab <- spacings_tab_grouped %>%
  mutate(Nb_datasets = length(unique(Dataset))) %>%
  ungroup() %>%
  mutate(
    Core_logo = file.path(SPACING_FOLDER, paste0("core_subcluster_", Subcluster, ".png")),
    Cobinder_logo = file.path(SPACING_FOLDER, paste0("cobinder_subcluster_", Subcluster, ".png")),
    Subcluster = paste0("Co-binding ", Subcluster)) %>%
  select(Subcluster, Core_logo, Cobinder_logo, Nb_datasets, Location, Spacer_length) %>%
  unique() %>%
  cbind(nb_regions_in_group) %>%
  mutate(Cobinder_proportion = group_length/all_group_length) %>%
  select(-all_group_length, -group_length) %>%
  dplyr::mutate(Cobinder_proportion = format(round(Cobinder_proportion, digits = 3), nsmall = 3))

##--------------------------------------------------------------##
## Adding group proportion info into the spacings summary table ##
##--------------------------------------------------------------##

spacings_tab_nb_regions <- left_join(
  spacings_tab_nb_regions, spacing_tab,
  by = c(
    "Subcluster" = "Subcluster",
    "Location" = "Location",
    "Spacer_length" = "Spacer_length")) %>%
  arrange(Subcluster, Location, Spacer_length)

## Exporting
fwrite(
  spacings_tab_nb_regions,
  file = file.path(SPACING_FOLDER, "spacings_summary.tab"),
  sep = "\t", col.names = TRUE)
save(
  spacings_tab_nb_regions,
  file = file.path(SPACING_FOLDER, "spacings_summary.RData"))

#############################
## Defining plot variables ##
#############################

## Modifying spacer lengths to shift plotting to left and right
## and to achieve empty space in the middle for logos
spacing_tab$original_spacers <- ifelse(
  spacing_tab$Location == "left",
  yes = -(spacing_tab$Spacer_length),
  no  = spacing_tab$Spacer_length)
spacing_tab$Spacer_length <- ifelse(
  spacing_tab$Location == "left",
  yes = -(spacing_tab$Spacer_length + 2),
  no  = (spacing_tab$Spacer_length + 2))

## Various params:
text_size <- 16

nb_subclusters <- length(unique(spacing_tab$Subcluster))
max_nb_datasets <- max(spacing_tab$Nb_datasets)
min_spacer <- min(spacing_tab$Spacer_length)
max_spacer <- max(spacing_tab$Spacer_length)

#####################################
## Defining axis breaks and labels ##
#####################################

##### General labels and breaks #####
x_axis_breaks <- spacing_tab %>%
  dplyr::select(Location, Spacer_length) %>%
  unique() %>%
  arrange(Spacer_length)
x_axis_breaks <- x_axis_breaks$Spacer_length
x_axis_breaks <- c(0, x_axis_breaks[x_axis_breaks != 0])

x_axis_labels <- spacing_tab %>%
  dplyr::select(Location, original_spacers) %>%
  unique() %>%
  arrange(original_spacers)
x_axis_labels <- c("Anchor", x_axis_labels$original_spacers)

##### CORE placeholder data #####
## Adding some empty values to get the points invisibly plotted
## This is a placeholder to plot the core motifs
core_logos_placeholder_data <-
  data.frame(
    Subcluster = spacing_tab$Subcluster[1],
    Core_logo = c("CORE_MIN", "None", "CORE_MAX"),
    Cobinder_logo = "None",
    Nb_datasets = NA,
    Location = "None",
    Spacer_length = c(-1, 0, 1),
    Cobinder_proportion = NA)

spacing_tab <- spacing_tab %>% 
  dplyr::select(-original_spacers)
spacing_tab_for_plot <- rbind(spacing_tab, core_logos_placeholder_data)

##### Masking giant empty spaces in X axis when co-binder is super far away #####
## Adding breaks to avoid a lot of empty space when spacer is continuous
## So we will make spacer discrete and add specific labels
full_breaks_vector <- seq(
  min(sort(x_axis_breaks)),
  max(sort(x_axis_breaks))
)
skipped_breaks <- setdiff(full_breaks_vector, x_axis_breaks)
skipped_breaks <- setdiff(skipped_breaks, c(-1, 1))

if (length(skipped_breaks) > 0) {

  vector_to_compare <- sort(c(x_axis_breaks, -1, 1))
  additional_breaks <- c()
  additional_labels <- c()

  for (skipped_break in skipped_breaks) {

    if (((skipped_break - 1) %in% vector_to_compare) & ((skipped_break + 1) %in% vector_to_compare)) {

        additional_breaks <- c(additional_breaks, skipped_break)
        additional_labels <- c(
          additional_labels,
          ifelse(skipped_break < 0, yes = (skipped_break + 2), no = (skipped_break - 2)))

    } else {

      if (is.null(tail(additional_labels, n = 1))) {
        additional_breaks <- c(additional_breaks, skipped_break)
        additional_labels <- c(additional_labels, "//")

      } else if ((!((skipped_break - 1) %in% additional_breaks)) & (!((skipped_break + 1) %in% additional_breaks))) {

        if ((skipped_break - 1) %in% vector_to_compare) {

          additional_breaks <- c(additional_breaks, skipped_break)
          additional_labels <- c(additional_labels, "//")

        } else {

          if (tail(additional_labels, n = 1) != "//") {

          additional_breaks <- c(additional_breaks, skipped_break)
          additional_labels <- c(additional_labels, "//")
          }
        }


      } 
    }
  }

  x_axis_breaks <- c(x_axis_breaks, additional_breaks)
  x_axis_labels <- c(x_axis_labels, additional_labels)

  ## Adding additional breaks placeholder data:
  additional_placeholder_data <- 
    data.frame(
      Subcluster = spacing_tab$Subcluster[1],
      Core_logo = "None",
      Cobinder_logo = "None",
      Nb_datasets = NA,
      Location = "None",
      Spacer_length = additional_breaks,
      Cobinder_proportion = NA)

  spacing_tab_for_plot <- rbind(spacing_tab_for_plot, additional_placeholder_data)
}

##### CO-BINDER placeholder data #####
## Adding placeholder data for the co-binder logos
cobinder_logos_placeholder_data <- spacing_tab %>%
  dplyr::group_by(Location) %>%
  dplyr::mutate(max_spacer = max(abs(Spacer_length))) %>%
  dplyr::mutate(
    Subcluster = spacing_tab$Subcluster[1],
    Core_logo = "None",
    Cobinder_logo = "None",
    Nb_datasets = NA,
    Cobinder_proportion = NA) %>%
  unique() %>%
  dplyr::mutate(Spacer_length = paste0(c(
    abs(max_spacer) + 1, abs(max_spacer) + 2, abs(max_spacer) + 3), collapse = "::")) %>%
  dplyr::ungroup() %>%
  unique()

cobinder_logos_placeholder_data <- tidyr::separate_rows(
  cobinder_logos_placeholder_data,
  Spacer_length,
  sep = "::"
) %>% unique()

cobinder_logos_placeholder_data$Spacer_length <- ifelse(
  cobinder_logos_placeholder_data$Location == "left",
  yes = -(as.numeric(cobinder_logos_placeholder_data$Spacer_length)),
  no  = as.numeric(cobinder_logos_placeholder_data$Spacer_length))

cobinder_logos_placeholder_data <- cobinder_logos_placeholder_data %>%
  dplyr::group_by(Location) %>%
  dplyr::mutate(median_spacer = median(Spacer_length)) %>%
  dplyr::mutate(Cobinder_logo = ifelse(
    Spacer_length < median_spacer, 
    yes = "COBINDER_MIN", no = "COBINDER_MAX")) %>%
  dplyr::mutate(Cobinder_logo = ifelse(
    Spacer_length == median_spacer, 
    yes = "None", no = Cobinder_logo)) %>%
  dplyr::select(-max_spacer, -median_spacer) %>%
  ungroup()

spacing_tab_for_plot <- rbind(spacing_tab_for_plot, cobinder_logos_placeholder_data)

## Adding breaks and labels for cobinders:
cobinder_logos_placeholder_data <- cobinder_logos_placeholder_data %>%
  dplyr::group_by(Location) %>%
  dplyr::mutate(label_coord = median(as.numeric(as.character(Spacer_length)))) %>%
  ungroup() %>%
  dplyr::select(label_coord) %>%
  unique() %>%
  dplyr::mutate(label = "Co-binding\npattern")

cobinder_logo_breaks <- cobinder_logos_placeholder_data$label_coord
cobinder_logo_labels <- cobinder_logos_placeholder_data$label

x_axis_breaks <- c(x_axis_breaks, cobinder_logo_breaks)
x_axis_labels <- c(x_axis_labels, cobinder_logo_labels)

## Order of subclusters:
subclusters_order <- spacing_tab %>%
  arrange(Location, Spacer_length) %>%
  select(Subcluster, Location) %>%
  unique()

#######################################
## Plotting the spacers as tile plot ##
#######################################

spacing_tab_for_plot <- spacing_tab_for_plot %>%
  dplyr::mutate(Spacer_length = as.factor(Spacer_length))

message("; Plotting spacers")

spacing_plot <- ggplot(
    data = spacing_tab_for_plot,
    aes(
      x = Spacer_length, 
      y = Subcluster, 
      fill = Nb_datasets)) +
  geom_tile(alpha = 1, width = 0.95, height = 0.95, color = "#ffffff00") +
  coord_equal() +
  geom_point(
    aes(size = as.numeric(Cobinder_proportion)),
    na.rm = TRUE) +
  geom_text(
    aes(label = Cobinder_proportion), 
    size = 5, 
    fontface = "bold", 
    vjust = 0, nudge_y = 0.2, na.rm = TRUE) +
  scale_x_discrete(
    breaks = x_axis_breaks,
    labels = x_axis_labels,
    expand = c(0, 0)) +
  scale_fill_distiller(
    palette  = "RdPu", 
    direction = +1,
    limits   = c(0, max_nb_datasets),
    name     = "Datasets",
    breaks   = c(0, max_nb_datasets),
    na.value = NA) +
  scale_size_continuous(
    name   = "Co-binding\nproportion",
    limits = c(0, 1),
    breaks = c(0, 0.5, 1),
    labels = c(0, 0.5, 1)) +
  scale_y_discrete(
    limits = subclusters_order$Subcluster,
    expand = c(0, 0.4)) +
  geom_hline(
    yintercept = c(0.5 + as.numeric(factor(unique(spacing_tab_for_plot$Subcluster)))), 
    color = "darkgrey",
    linewidth = 0.3,
    linetype = 2) +
  labs(x = "", y = "") +
  theme_bw() +
  theme(
    legend.position = "left",
    legend.box = "vertical",
    legend.text = element_text(size = text_size),
    legend.title = element_text(size = text_size),
    axis.text.x = element_text(
      size = text_size),
    axis.text.y = element_text(size = text_size),
    axis.ticks.y = element_blank(),
    axis.ticks.x = element_line(
      color = ifelse(
        x_axis_labels == "//",
        yes = "#ffffff00",
        no  = "darkgrey")),
    panel.grid.major.x = element_blank(),
    panel.grid.minor.x = element_blank(),
    panel.grid.major.y = element_blank(),
    axis.line.y = element_blank(),
    axis.line.x = element_line(
      color = "darkgrey"),
    panel.background = element_rect(fill = "transparent",
                                  colour = NA_character_),
    panel.border = element_blank()) +
  guides(
    size = guide_legend(override.aes = list(order = 2)), 
    col = guide_legend(override.aes = list(order = 1)))

##########################
## Inserting CORE logos ##
##########################

subclusters_order_for_cores <- subclusters_order %>%
  dplyr::select(Subcluster) %>%
  unique()

message("; Adding CORE motif logos")
for (i in 1:nrow(subclusters_order_for_cores)) {

  subcluster <- subclusters_order_for_cores[i, ]
  
  logo <- subset(spacing_tab, Subcluster == subcluster)[1,]$Core_logo
  logo_png <- png::readPNG(logo)
  
  ## Coordinates:
  xmin_coord <- subset(spacing_tab_for_plot, Core_logo == "CORE_MIN")$Spacer_length
  xmax_coord <- subset(spacing_tab_for_plot, Core_logo == "CORE_MAX")$Spacer_length
  ymin_coord <- i - 0.4
  ymax_coord <- i + 0.4

  spacing_plot <- spacing_plot +
    ggplot2::annotation_raster(
      logo_png,
      xmin = xmin_coord, 
      xmax = xmax_coord,
      ymin = ymin_coord, 
      ymax = ymax_coord,
      interpolate = TRUE)
}

##############################
## Inserting COBINDER logos ##
##############################

message("; Adding CO-BINDER motif logos")
for (j in 1:nrow(subclusters_order)) {

  subcluster <- subclusters_order[j, ]$Subcluster
  location   <- subclusters_order[j, ]$Location

  subcluster_idx <- which(subclusters_order_for_cores == subcluster)
  
  ## Reading logo:
  logo <- subset(spacing_tab, Subcluster == subcluster)[1,]$Cobinder_logo
  logo_png <- png::readPNG(logo)

  ## Determining positions of logo:
  logo_coordinates <- spacing_tab_for_plot %>%
    dplyr::filter(
      Location == location,
      Core_logo == "None")
  xmin_coord <- subset(logo_coordinates, Cobinder_logo == "COBINDER_MIN")[1,]$Spacer_length
  xmax_coord <- subset(logo_coordinates, Cobinder_logo == "COBINDER_MAX")[1,]$Spacer_length

  ## Inserting logo:
  spacing_plot <- spacing_plot +
    ggplot2::annotation_raster(
      logo_png,
      xmin = xmin_coord,
      xmax = xmax_coord,
      ymin = (subcluster_idx - 0.4), ymax = (subcluster_idx + 0.4),
      interpolate = TRUE)
}

####################
## Exporting plot ##
####################

message("; Exporting spacing plot for ", FAMILY_NAME)
TF_COBINDER_SPACER_PLOT_PDF <-  file.path(SPACING_FOLDER, paste0("TF_cobinder_spacer_plot_", FAMILY_NAME, ".pdf"))

estimated_plot_width  <- length(x_axis_breaks) + 10
estimated_plot_height <- nb_subclusters + 3

ggsave(
  filename  = TF_COBINDER_SPACER_PLOT_PDF,
  plot      = spacing_plot,
  width     = estimated_plot_width,
  height    = estimated_plot_height,
  limitsize = FALSE)

## Save as RData:
TF_COBINDER_SPACER_PLOT_RDATA <-  file.path(SPACING_FOLDER, paste0("TF_cobinder_spacer_plot_", FAMILY_NAME, ".RData"))
save(
  spacing_plot,
  estimated_plot_width,
  estimated_plot_height,
  file = TF_COBINDER_SPACER_PLOT_RDATA)

### End of draw_spacers_heatmap_extract_regions.R ###