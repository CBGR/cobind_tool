from scipy.special import xlogy
import numpy as np
import seqlogo
import os
import sys

## Cores and cobinders are clustered independently in the clustering step.
## In this script we form sublusters by combining the two clustering partitions, then we find archetypes (core and cobinder motifs that represent each subcluster) and
## for each subcluster we then compute spacers between core and cobinder archetypical motifs.

## Syntax : python compute_spacers <core_folder> <motif_folder> <family_name> <output_folder>

## The input folders are the folders containing the output files of the rsat matrix_clustering algorithm.
## In the <output_folder> the core and cobinder archetypical motifs are created together with a .txt file with the spacings information, which is further used for drawing a heatmap.

#---------------------#
# Functions
#---------------------#

### 1. ### Compute the renyi entropy of a nucleotide, which corresponds to the IC when alpha=1.
## This is used for the trimming before combining the motifs.
def pfmnuc2renyi(nuc,alpha=1):
	prob=nuc/np.sum(nuc)
	if alpha!=1:
		return(2-1/(1-alpha)*log(np.sum(np.power(prob,alpha)),2))
	else:
		return(2+np.sum(xlogy(prob,prob)/np.log(2)))

### 2. ### Compute the renyi entropy of a pfm.
## This is also used for the trimming before combining the motifs.
def pfm2renyi(pfm,alpha=1):
	return([pfmnuc2renyi(nuc,alpha) for nuc in pfm])

### 3. ### The main program.
## Forms sublusters by combining the two clustering partitions, then we find archetypes (core and cobinder motifs that represent each subcluster) and
## for each subcluster we then compute spacers between core and cobinder archetypical motifs.
def motifs_and_distances(core_folder,cobinder_folder,family_name,output_folder):

	convert_matrix = os.path.join(os.getenv('COBIND_CLUSTERING'), "convert-matrix.R")

	with open("{}/{}_core_tables/clusters.tab".format(core_folder,family_name)) as core_clusters_file:
		core_clusters=[line.rstrip().split("	")[1].split(',') for line in core_clusters_file.readlines()[1:]]
	with open("{}/{}_core_tables/alignment_table.tab".format(core_folder,family_name)) as core_align_file:
		core_align=[line.rstrip().split("	") for line in core_align_file.readlines()]
	with open("{}/{}_cobinder_tables/clusters.tab".format(cobinder_folder,family_name)) as cobinder_clusters_file:
		cobinder_clusters=[line.rstrip().split("	")[1].split(',') for line in cobinder_clusters_file.readlines()[1:]]
	with open("{}/{}_cobinder_tables/alignment_table.tab".format(cobinder_folder,family_name)) as cobinder_align_file:
		cobinder_align=[line.rstrip().split("	") for line in cobinder_align_file.readlines()]

	subclusters_list=[]
	common_core_trim_list=[]

	merge_thresh=0 ##Trimmimg threshold before combining the motifs.
	final_thresh=0.25 ##Trimming threshold after combining the motifs.
	# final_thresh=0.1

	## Going through each core and cobinder cluster to see, whether they have common motifs to form a subcluster.

	print("; Computing the spacers between anchor and co-binder motifs.")
	for cclust in core_clusters:
		# ##Computing common trim values for the core cluster. Used by option 4 of trimming
		# ppm_list=[]
		# for core_str in cclust:
		# 	print(core_str)
		# 	str_matrix=os.popen("{} -from tf -to cluster-buster -i {}/{}_core_aligned_logos/{}_aligned.tf -return counts".format(convert_matrix,core_folder,family_name,core_str)).read()
		# 	matrix=np.array([line.rstrip().split('\t') for line in str_matrix.splitlines()[1:]],dtype='float')
		# 	print(matrix)
		# 	matrix_sums=np.sum(matrix,axis=1)
		# 	matrix[np.where(matrix_sums==0)[0]]=[250,250,250,250]
		# 	ppm=matrix/np.sum(matrix,axis=1)[:,None]
		# 	print(ppm)
		# 	ppm_list.append(ppm)
		# merged_ppm=np.mean(ppm_list,axis=0)
		# merged_info_cont=np.array(pfm2renyi(merged_ppm))

		# # print("merged_ppm")
		# # print(merged_ppm)
		# if len(merged_ppm)==1:
		# 	#No trim on the cores:
		# 	trim_values=[0,len(merged_info_cont)-1]
		# else:
		# 	#Apply trim on cores:
		# 	trim_values=[np.min(np.where(merged_info_cont>final_thresh)[0]),np.max(np.where(merged_info_cont>final_thresh)[0])]
		# print("trim_values")
		# print(trim_values)
		# #Apply trim on cores:
		# # trim_values=[np.min(np.where(merged_info_cont>final_thresh)[0]),np.max(np.where(merged_info_cont>final_thresh)[0])]
		# #No trim on the cores:
		# # trim_values=[0,len(merged_info_cont)-1]

		for mclust in cobinder_clusters:
			subcluster=[] ## When the cobinder is not reversed.
			subcluster_rc=[] ## When the aligned cobinder is reversed with regard to the aligned core, then it forms a different subcluster.
			for cobinder_str in mclust:
				cobinder_str_corr=cobinder_str.split("_trimmed_")[0].replace('cobinder_pfm_','') ## Extracting the unique motif root (name of the dataset, k components and the component number).
				for core_str in cclust:
					core_str_corr=core_str.split("_trimmed_")[0].replace('core_pfm_','') ## Extracting the unique motif root (name of the dataset, k components and the component number).
					if cobinder_str_corr==core_str_corr: ## Checking if the core and cobinder are associated (if they have the same motif root).
					## Recovering all the information about alignment.
						experiment_info=[core_str,cobinder_str] ## First two columns in spacings.txt
						for line in core_align:
							if line[0]==core_str:
								experiment_info += line[3:7]
						for line in cobinder_align:
							if line[0]==cobinder_str:
								experiment_info += line[3:7]
						if '_left_' in core_str_corr.split('.')[-1]:
							experiment_info.append('left')
						elif '_right_' in core_str_corr.split('.')[-1]:
							experiment_info.append('right')
						else:
							experiment_info.append('could not find direction')
						experiment_info+=[0,0,0,'no direction','']

						if experiment_info[2] == experiment_info[6]: ## Checking, if the aligned core and cobinder are in the same direction.
							subcluster.append(experiment_info)
						else:
							subcluster_rc.append(experiment_info)
## After this we end up with two sets of core-cobinder pairs.
## If they are not empty, add them to the list of subclusters:
			if len(subcluster)>0:
				subclusters_list.append(subcluster)
				# common_core_trim_list.append(trim_values)
			if len(subcluster_rc)>0:
				subclusters_list.append(subcluster_rc)
				# common_core_trim_list.append(trim_values)
			# print("common_core_trim_list")
			# print(common_core_trim_list)

## Making the output directory.
	if not os.path.exists(output_folder):
		os.makedirs(output_folder)

## Building core and cobinder archetypical motif for each subcluster.
	print("; Building anchor and cobinder motifs for each subcluster.")
	trim_values=[]
	for i in range(len(subclusters_list)):
		subcluster=subclusters_list[i]

		## Building CORE archetypical motif.
		max_trim_list=[]
		min_trim_list=[]
		ppm_list=[]
		for motif in subcluster:
			## Building a ppm for each core motif in the subcluster:
			# str_matrix=os.popen("{} -from tf -to cluster-buster -i {}/{}_core_aligned_logos/{}_aligned.tf -return counts".format(convert_matrix,core_folder,family_name,motif[0])).read()

			str_converted_matrix="{}/{}_core_motifs/individual_motifs_with_gaps/{}_oriented.txt".format(core_folder,family_name,motif[0])

			os.system("Rscript {} -i {}/{}_core_motifs/individual_motifs_with_gaps/{}_oriented.tf --from tf --to cluster-buster -o {}".format(convert_matrix,core_folder,family_name,motif[0],str_converted_matrix))

			str_matrix=open(str_converted_matrix).read()
			str_matrix = [line.rstrip().split('\t') for line in str_matrix.splitlines()[1:]]
			mat_len = len(str_matrix)
			matrix=np.array(str_matrix[0:mat_len-1], dtype='float')

			matrix_sums=np.sum(matrix,axis=1)
			matrix[np.where(matrix_sums==0)[0]]=[250,250,250,250]
			ppm=matrix/np.sum(matrix,axis=1)[:,None]
			ppm_list.append(ppm)
		
			### Option 1 : Finding trim indices (start and end indices) for each motif, based on IC threshold
			# info_cont=np.array(pfm2renyi(ppm))
			# if len(np.where(info_cont>merge_thresh)[0])>0: ## Removing empty positions. If more positions are wished to be removed, then info_cont>0 should be higher than 0.
			# 	min_trim_list.append(np.min(np.where(info_cont>merge_thresh)[0])) ## If more positions are wished to be removed, then info_cont>0 should be higher than 0.
			# 	max_trim_list.append(np.max(np.where(info_cont>merge_thresh)[0])) ## If more positions are wished to be removed, then info_cont>0 should be higher than 0.
			# else:
			# 	min_trim_list.append(0)
			# 	max_trim_list.append(len(info_cont)-1)

			### Option 2 : Trimming only the added alignement colums
			if len(np.where(matrix_sums>0)[0])>0:
				min_trim_list.append(np.min(np.where(matrix_sums>0)[0]))
				max_trim_list.append(np.max(np.where(matrix_sums>0)[0]))
			else:
				min_trim_list.append(0)
				max_trim_list.append(len(matrix_sums)-1)

			### Option 3 : No trimming
			# min_trim_list.append(0)
			# max_trim_list.append(len(matrix_sums)-1)
		# print(ppm_list)
		## Compute the most restrictive trim indices to combine the motifs. Enable for option 1,2,3
		min_trim_core=np.max(min_trim_list)
		max_trim_core=np.min(max_trim_list)

		# ###Option 4 : Common core trimming
		# common_core_trim = common_core_trim_list[i]
		# min_trim_core=common_core_trim[0]
		# max_trim_core=common_core_trim[1]
		# # print("common_core_trim")
		# # print(common_core_trim)
		# # print("min_trim_core")
		# # print(min_trim_core)
		# # print("max_trim_core")
		# # print(max_trim_core)
		merged_ppm=np.mean(ppm_list,axis=0) ## Combining: a mean of ppms.
		trimmed_ppm_core=merged_ppm[min_trim_core:max_trim_core+1] ## Applying trim indices on the combined motif.

		# trim_merged_core=False ##Trimming on merged core can be disabled
		# ## If this is set to false, and trimming before merging is also disabled (option 3) or common to a core cluster (option 4), it makes sure that all the core motifs from the same core cluster, but from separate sublusters, will have the same length
		trim_merged_core=True

		if trim_merged_core:
			merged_info_cont=np.array(pfm2renyi(trimmed_ppm_core))
			if len(np.where(merged_info_cont>final_thresh)[0])>0:
					min_trim_core+=np.min(np.where(merged_info_cont>final_thresh)[0])
					max_trim_core-=len(merged_info_cont)-1 - np.max(np.where(merged_info_cont>final_thresh)[0])
			trimmed_ppm_core=merged_ppm[min_trim_core:max_trim_core+1] ## Applying trim indices on the combined motif.

		np.savetxt("{}/core_subcluster_{}.txt".format(output_folder,i),trimmed_ppm_core ,fmt='%f',header='>Core_subcluster_{}_m_{}'.format(i,len(subcluster)),comments='')
		ppm_object=seqlogo.Ppm(trimmed_ppm_core) ## Making the archetypical logo.
		seqlogo.seqlogo(ppm_object, ic_scale = True, format = 'png',size='xlarge', filename="{}/core_subcluster_{}.png".format(output_folder,i))
		## Creating reverse complement logo
		# ppm_object_rc  = seqlogo.Ppm(trimmed_ppm_core[::-1,::-1])
		# seqlogo.seqlogo(ppm_object_rc, ic_scale = True, format = 'png',size='xlarge', filename="{}/core_subcluster_{}_rc.png".format(output_folder,i))

		## Building COBINDER archetypical motif.
		max_trim_list=[]
		min_trim_list=[]
		ppm_list=[]

		for motif in subcluster:
			if motif[2]==motif[6]: ## Checking if aligned cobinders orientation is consistent with the aligned core.
				suffix=''
			else:
				suffix='_rc' ## If the aligned cobinder is not in the same direction as aligned core, then we take reverse complement of the aligned cobinder motif, when reading ppm.

			# str_matrix=os.popen("{} -from tf -to cluster-buster -i {}/{}_cobinder_aligned_logos/{}_aligned{}.tf -return counts".format(convert_matrix,cobinder_folder,family_name,motif[1],suffix)).read() ## suffix tells if the reversed or not reversed should be taken.

			str_converted_matrix="{}/{}_cobinder_motifs/individual_motifs_with_gaps/{}_oriented{}.txt".format(cobinder_folder,family_name,motif[0],suffix)
			
			os.system("Rscript {} -i {}/{}_cobinder_motifs/individual_motifs_with_gaps/{}_oriented{}.tf --from tf --to cluster-buster -o {}".format(convert_matrix,cobinder_folder,family_name,motif[1],suffix,str_converted_matrix))

			str_matrix=open(str_converted_matrix).read()
			str_matrix = [line.rstrip().split('\t') for line in str_matrix.splitlines()[1:]]
			mat_len = len(str_matrix)
			matrix=np.array(str_matrix[0:mat_len-1], dtype='float')

			# matrix=np.array([line.rstrip().split('\t') for line in str_matrix.splitlines()[1:]],dtype='float')

			matrix_sums=np.sum(matrix,axis=1)
			matrix[np.where(matrix_sums==0)[0]]=[250,250,250,250]
			ppm=matrix/np.sum(matrix,axis=1)[:,None]
			ppm_list.append(ppm)

			### Option 1 : Finding trim indices (start and end indices) for each motif, based on IC threshold
			# info_cont=np.array(pfm2renyi(ppm))
			# if len(np.where(info_cont>merge_thresh)[0])>0: ## Removing empty positions. If more positions are wished to be removed, then info_cont>0 should be higher than 0.
			# 	min_trim_list.append(np.min(np.where(info_cont>merge_thresh)[0])) ## If more positions are wished to be removed, then info_cont>0 should be higher than 0.
			# 	max_trim_list.append(np.max(np.where(info_cont>merge_thresh)[0])) ## If more positions are wished to be removed, then info_cont>0 should be higher than 0.
			# else:
			# 	min_trim_list.append(0)
			# 	max_trim_list.append(len(info_cont)-1)

			### Option 2 : Trimming only the added alignement colums
			# if len(np.where(matrix_sums>0)[0])>0:
			# 	min_trim_list.append(np.min(np.where(matrix_sums>0)[0]))
			# 	max_trim_list.append(np.max(np.where(matrix_sums>0)[0]))
			# else:
			# 	min_trim_list.append(0)
			# 	max_trim_list.append(len(matrix_sums)-1)

			### Option 3 : No trimming
			min_trim_list.append(0)
			max_trim_list.append(len(matrix_sums)-1)

		#Trimming indices
		min_trim_cobinder=np.max(min_trim_list)
		max_trim_cobinder=np.min(max_trim_list)

		merged_ppm=np.mean(ppm_list,axis=0)
		trimmed_ppm_cobinder=merged_ppm[min_trim_cobinder:max_trim_cobinder+1]


		merged_info_cont=np.array(pfm2renyi(trimmed_ppm_cobinder))
		if len(np.where(merged_info_cont>final_thresh)[0])>0:
				min_trim_cobinder+=np.min(np.where(merged_info_cont>final_thresh)[0])
				max_trim_cobinder-=len(merged_info_cont)-1 - np.max(np.where(merged_info_cont>final_thresh)[0])
		trimmed_ppm_cobinder=merged_ppm[min_trim_cobinder:max_trim_cobinder+1] ## Applying trim indices on the combined motif.
		# print("min_trim_cobinder")
		# print(min_trim_cobinder)
		# print("max_trim_cobinder")
		# print(max_trim_cobinder)
		# print("trimmed_ppm_cobinder")
		# print(trimmed_ppm_cobinder)

		np.savetxt("{}/cobinder_subcluster_{}.txt".format(output_folder,i),trimmed_ppm_cobinder ,fmt='%f',header='>Cobinder_subcluster_{}_m_{}'.format(i,len(subcluster)),comments='')
		ppm_object=seqlogo.Ppm(trimmed_ppm_cobinder)
		seqlogo.seqlogo(ppm_object, ic_scale = True, format = 'png',size='xlarge', filename="{}/cobinder_subcluster_{}.png".format(output_folder,i))
		## Creating reverse complement logo
		ppm_object_rc  = seqlogo.Ppm(trimmed_ppm_cobinder[::-1,::-1])
		# seqlogo.seqlogo(ppm_object_rc, ic_scale = True, format = 'png',size='xlarge', filename="{}/cobinder_subcluster_{}_rc.png".format(output_folder,i))


		## Saving all the trim indices for the subcluster.
		## These indices will be used in computing the spacings.
		trim_values.append([min_trim_core,max_trim_core,min_trim_cobinder,max_trim_cobinder])

	## Computing the spacings.
		subcluster=subclusters_list[i]
		trim=trim_values[i]
		for motif in subcluster: ## motif is the line with all the alignment information (basically a line in spacings.txt).
			## For core and cobinder we compute independently the distance between the aligned/trimmed motif and the edge between original core and cobinder sequences.
			core_precluster_trim=motif[0].split('_trimmed_')[1].split('_')[0:2] ## Recovering the trim values before the clustering.
			cobinder_precluster_trim=motif[1].split('_trimmed_')[1].split('_')[0:2]

			direction=motif[10] ## Direction of the NMF.

			## Where is the cobinder relative to the core during NMF (side of the flanks).
			if direction=='right':
				core_distance=int(core_precluster_trim[1]) ## Right trim for the core.
				cobinder_distance=int(cobinder_precluster_trim[0]) ## Left trim for the cobinder.
			else:
				core_distance=int(core_precluster_trim[0]) ## The opposite: left for the core.
				cobinder_distance=int(cobinder_precluster_trim[1]) # Right for the cobinder.

			## Where is the core relative to the cobinder after alignment.
			if (direction=='right' and motif[6]=='D') or (direction=='left' and motif[6]=='R'): ## This means that the core is on the left of the aligned cobinder.
				cobinder_distance-=int(motif[7]) ## We remove the gaps that were added on the left of the cobinder during the alignment.
			else: ## This means that the core is on the right of the cobinder.
				cobinder_distance-=int(motif[8]) ## We remove the gaps that were added on the right of the cobinder during the alignment.

			## Where is the cobinder relative to the core after alignment.
			## To remove the gaps added to the core during the alignment and to remove trim that was done to combine the motifs.
			if (direction=='right' and motif[2]=='D') or (direction=='left' and motif[2]=='R'):  ## This means that the cobinder is on the right of the aligned core.
				core_distance+=int(motif[5])-1-trim[1]-int(motif[4])
				cobinder_distance+=trim[2]
				core_direction='right' ## Save the direction of the cobinder relative to the aligned core (this will added as a column in the spacings.txt).
			else:
				core_distance+=trim[0]-int(motif[3])
				cobinder_distance+=int(motif[9])-1-trim[3]
				core_direction='left'
			motif[11]=core_distance
			motif[12]=cobinder_distance
			total_distance=core_distance+cobinder_distance
			motif[13]=total_distance
			motif[14]=core_direction
			motif[15]=str(i)

			if total_distance<0:
			# if cobinder_distance<0:
				if core_distance<0:
					
					partial_cobinder_ppm=trimmed_ppm_cobinder
					if core_direction=='right':
						partial_core_ppm=trimmed_ppm_core[:total_distance]
					else:
						partial_core_ppm=trimmed_ppm_core[-total_distance:]

				if cobinder_distance<0:

					partial_core_ppm=trimmed_ppm_core
					if core_direction=='right':
						partial_cobinder_ppm=trimmed_ppm_cobinder[-total_distance:]
					else:
						partial_cobinder_ppm=trimmed_ppm_cobinder[:total_distance]

				np.savetxt("{}/cobinder_subcluster_{}_{}.txt".format(output_folder,i,total_distance),partial_cobinder_ppm ,fmt='%f',header='>Cobinder_subcluster_{}_{}_m_{}'.format(i,total_distance,len(subcluster)),comments='')
				partial_ppm_object=seqlogo.Ppm(partial_cobinder_ppm)
				seqlogo.seqlogo(partial_ppm_object, ic_scale = True, format = 'png',size='xlarge', filename="{}/cobinder_subcluster_{}_{}.png".format(output_folder,i,total_distance))
				## Creating reverse complement logo
				partial_ppm_object_rc  = seqlogo.Ppm(partial_cobinder_ppm[::-1,::-1])
				# seqlogo.seqlogo(partial_ppm_object_rc, ic_scale = True, format = 'png',size='xlarge', filename="{}/cobinder_subcluster_{}_{}_rc.png".format(output_folder,i,total_distance))

				np.savetxt("{}/core_subcluster_{}_{}.txt".format(output_folder,i,total_distance),partial_core_ppm ,fmt='%f',header='>Core_subcluster_{}_{}_m_{}'.format(i,total_distance,len(subcluster)),comments='')
				ppm_object=seqlogo.Ppm(partial_core_ppm)
				seqlogo.seqlogo(ppm_object, ic_scale = True, format = 'png',size='xlarge', filename="{}/core_subcluster_{}_{}.png".format(output_folder,i,total_distance))
				## Creating reverse complement logo
				ppm_object_rc  = seqlogo.Ppm(partial_core_ppm[::-1,::-1])
				# seqlogo.seqlogo(ppm_object_rc, ic_scale = True, format = 'png',size='xlarge', filename="{}/core_subcluster_{}_{}_rc.png".format(output_folder,i,total_distance))


				motif[13]=0
				motif[15]+='_{}'.format(total_distance)

	## Writing a spacings.txt table.
	with open("{}/spacings.txt".format(output_folder),'w+') as file:
		for i in range(len(subclusters_list)):
			# print("subcluster : {}".format(i))
			subcluster=subclusters_list[i]
			for motif in subcluster:
				# print(motif)
				file.write("	".join([str(data) for data in motif])+'\n')

#---------------------#
# Running the program.
#---------------------#

if __name__=='__main__':
	arguments=sys.argv

	core_folder=arguments[1]
	cobinder_folder=arguments[2]
	family_name=arguments[3]
	output_folder=arguments[4]

	motifs_and_distances(core_folder,cobinder_folder,family_name,output_folder)
