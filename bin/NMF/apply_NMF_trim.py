import sys
from NMF_script import *

### Run NMF on the sequences contained in a FASTA file. If a minority of sequences are of different length, those ones are removed.

### Syntax example:
        # python apply_NMF_trim.py \
        #     <fasta_path> \
        #     <output_folder> \
        #     <score_folder> \
        #     <experiment_name> \
        #     <n_components> \
        #     <gini_threshold> \
        #     <kim_park_threshold> \
        #     <fasta_core> (optional)

### A summary txt file with name "<experiment_name>_f<flank_size>_c<n_components>.txt" is always created in <score_folder>.

### If motifs are found, a folder <experiment_name>/<n_components>_components with motif information is created in <output_folder>.

## If a core fasta path is given as the last argument, outputs the core trimmed pfm corresponding to the found motifs (otherwise core pfms are not outputed).

#---------------------#
# Running the script.
#---------------------#

if __name__=='__main__':
	arguments=sys.argv

	fasta_path=arguments[1]
	output_folder=arguments[2]
	score_folder=arguments[3]
	experiment_name=arguments[4]
	n_components=int(arguments[5])
	gini_threshold=float(arguments[6])
	kim_park_threshold=float(arguments[7])

	trim_threshold=0 ## float(arguments[6])

	if len(arguments)>=8:
		core_fasta_path=arguments[8]
		core_sequences=fasta2seq(core_fasta_path)
	else:
		core_sequences=None

	## Converting input sequences to one-hot encoded object:
	sequences=fasta2seq(fasta_path)

	apply_NMF_trim(output_folder,score_folder,experiment_name,sequences,n_components,trim_threshold,core_sequences,full_output=False,gini_threshold=gini_threshold,kim_park_threshold=kim_park_threshold)

## End of script ##