# COBIND - a pipeline to discover co-binding motifs *de novo* #

2024 August 20

This is a README that shows how to use the **COBIND** pipeline to identify the co-binding patterns in the proximity of reference regions or sequences. The tool uses coordinates of reference regions (anchors) in BED format or nucleotide sequences that contain reference in the center and surrounding sequences where to look for co-bindign patterns. The pipeline returns discovered patterns with information about in how many datasets they were found and how many spacers are between the anchor and discovered pattern.

![COBIND overview](docs/figures/main_method_scheme.png){width = 150 height = 100}

**Figure 1.** Overview of the COBIND pipeline.

The algorithm used to discover DNA sequence patterns is **non-negative matrix factorization (NMF)**. The idea was inspired by the package [seqArchR](https://bioconductor.org/packages/release/bioc/html/seqArchR.html) developed by Sarvesh Nikumbh. The algorithm takes one-hot encoded genomic sequences of the same length and factorizes them into two types of matrices - *amplitude* and *pattern*. Depending on a number of components that was given as a hyperparamater, we can build sequence motifs out of the two resulting matrices.

![NMF for motif discovery](docs/figures/nmf_explained.png){width = 150 height = 100}

**Figure 2.** Non-negative matrix factorization (NMF) application on genomic sequences to discover transcription factor binding motifs.

## Installation ##

### Download/Clone the repository ###

Clone the git repo from BitBucket:

```bash
git clone https://username@bitbucket.org/CBGR/cobind_tool.git

cd cobind_tool
```

## Quick start ##

All neccesary software is available in the [**COBIND container**](https://hub.docker.com/r/cbgr/cobind) and the Snakefile is prepared to run using that software. However, to run the Snakemake pipeline, you must have Snakemake already installed. For installation follow the instructions [*here*](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html)

We provide a **config.yaml** file with the default parameters, which is prepared to use with a demo TFBSs dataset. We also provide an example file with TF-family information.

**NOTE!** The user must provide the human genome files (*hg38.chrom.sizes* and *hg38.fa*) themselves, if BED input is used. See FAQ how to get them.

Users can adapt the config files to their input.

Once you set the *config.yaml* file, run **Snakefile** script to launch the pipeline.

```bash
## Assuming you are in the cobind_tool folder.

snakemake --use-singularity --singularity-args '\-e' --cores 1
```

### Example output ###

A folder with example output (not complete output, but main plots described under "Output") is available as part of the repository. The folder is called:

```unix
example_output
```

For this example CTCF and GATA TFBSs in BED format were used.

## Setting up analysis with your data ##

### Input files ###

**COBIND** requires the following files as input input: (1) a collection of UniBind TFBSs in BED format, (2) human genome in FASTA format, (3) human genome chromosomes size file and (4) a table with information about TF families or TF classes or individual TFs.

  1. Anchor regions in BED format or FASTA sequences (anchors plus flanks) *(mandatory)*;
  2. Genome in FASTA format *(mandatory with BED input)*;
  3. Chromosomes sizes file *(mandatory with BED input)*;
  4. A .txt file with input file groups (for example, files grouped by a TF family) *(mandatory)*.

**NOTE!** Make sure that TFBSs, human genome and chrom sizes files were generated using the same human genome assembly version (e.g. hg38).

#### Regions in BED or FASTA format **(mandatory input)** ####

Input file names must have a ".bed" or ".fa" extension:

```bash
{file_name}.bed

## or

{file_name}.fa
```

It is important to synchronize the file names with your input family/class table (more info on this bellow).

**NOTE!** NO dots " . " (except in the extensions ".bed" or ".fa") are tolerated in the file name.

**NOTE!** File name length should be limited to and not exceed 65 characters (without extension).

**NOTE!** In the different files within the same group, the length of the regions must be the same.

**NOTE!** If FASTA is used as an input, the pipeline will divide provided regions into left and right flanking sequences and anchor. Therefore, the size of each sequence must be the same and anchor should be in the center. Flank size parameter should reflect the intended flanking region length.

If any of these problems will be found, the pipeline will produce errors and indicate, which files needs to be revisited before launching the pipeline again.

For BED input:

A. Required columns (columns should NOT be named):

* 1st - Chromosome
* 2nd - Start
* 3rd - End
  
B. Optional columns:

* 4th - TF_motif
* 5th - Motif score
* 6th - Strand

##### Examples of input region files #####

```bash
## 3-column BED file:
chr1 960585 960596
chr1 1064322 1064333
chr1 1116374 1116385
chr1 1232053 1232064
chr1 1353914 1353925

## 6-column BED file:
chr1 960585 960596 FOXP2_GAGTGAGCGAC 89    +
chr1 1064322 1064333 FOXP2_AGCGCTGACGC 92      -
chr1 1116374 1116385 FOXP2_GGCGTTGCCGG 91.3 -
chr1 1232053 1232064 FOXP2_GCGGAAGCGGT 90.4 +
chr1 1353914 1353925 FOXP2_GTTGTTTTTGT 92      -

## FASTA file:
>chr1:1005079-1005150(-)
CAGACACAATGGCCGGCCAGATAGGCTTCTCTTGATAAGGAGAGAACAGACAGGGGAGGAGGAAGCCGCAG
>chr1:1109872-1109943(-)
taactacagcgtcactttccataacttcctcctgataagacaccactggccacggactggttctgctggtc
>chr1:1185642-1185713(-)
TGCTTCCGGGGCCTGGCCGGGGTGACCAAGACAGATAAAAGTCCTTCGGCCAAGCCCCGTTTTTATCCCAG
>chr1:1614327-1614398(+)
tgcagggaaattcctcaggactgcagtattctagataagcagcttgcacaaggacgtcggcctggtaaggc
```

#### A .txt file with input file groups **(Mandatory input)** ####

The file should contain two tab-separated columns: one representing TF family/class or another preferred file grouping, while the second column should list the original file names without the extension. **NOTE!** This is why it is important that you synchronize the file names and this .txt table. File names must be separated by comma.

Example:

```bash
CTCF	example_CTCF
GATA2	example_GATA2
all_tfs	example_CTCF,example_GATA2
```

Corresponding files, for this example table would be:

```bash
example_CTCF.bed
example_GATA2.bed

## or

example_CTCF.fa
example_GATA2.fa
```

## Explaining the output ##

The pipeline outputs many files, but the most informative summaries of results can be found in the following files.

We advise to be cautious when interpreting A/T-rich patterns discovered as potential TF binding sites. We found from the random region analysis that such patterns are sometimes discovered in random regions (0.5% times it matches with what was discovered in the real data).


### Spacing summary heatmap & boxplot ###

#### A. HTML summary report

```bash
example_output/reports/CTCF/COBIND_analysis_report_CTCF.html
```

In order to conveniently review the results, the pipeline will produce the report in ```html``` format. This report includes the spacing plot (see B.), spacing summary table, information on available genomic regions and GREAT analysis results, if such was requested. All results are available in ```results``` directory, but could be dowloaded from the report as well.

#### B. Spacing heatmap plot will be found in a path like this (example output) ####

```bash
example_output/results/clustering_results/CTCF/spacings/TF_cobinder_spacer_plot_CTCF.pdf
```

In the spacing summary heatmap in the middle of the plots anchor motifs are represented, while on the right there is a co-binder candidate motif. The squares left and/or right indicate, where the co-binder motif was found relative to the displayed anchor motif (upstream or downstream) and the number of spaces in between the two motifs. The intensity of color reflects the number of datasets this specific combination of anchor and co-binder occurs (including the position and spacer). The proportion inside each square represent the proportion of sequences the co-binder was found in the number of those datasets.

![Spacing plot](example_output/results/clustering_results/CTCF/spacings/TF_cobinder_spacer_plot_CTCF.png){width = 150 height = 100}


**Figure 3.** Example of a spacing heatmap plot.

### Experiment summaries ###

#### A. Numbers of candidate co-binder motifs and the fractions of sequences are summarized in the density plot and histogram for all datasets (example output) ####

```bash
example_output/results/analysis_overall_summary/aggregated_density_plot_cobinder_specific.pdf

example_output/results/analysis_overall_summary/aggregated_histogram_plot_cobinder_specific.pdf
```

#### B. For individual TFs/families/classes separate histograms are also exported as a facet plot (example output) ####

```bash
example_output/results/analysis_overall_summary/histogram_plot_cobinder_specific.pdf
```

#### C. Finally, the table with discovered candidate co-binders, the numbers of datasets and the sequence proportion is exported here (example output) ####

```bash
example_output/results/analysis_overall_summary/cobinder_distribution_summary_table.txt
```

**NOTE!** For each dataset, disregarding whether any co-binders were found, NMF summary will be exported. Note that, if values for Gini coefficient, median, etc are "-2", it means that after filtering, the cluster, which leads to formation of a motif, did not contain 42 or more sequences, so there were not enough sequences to form a good candidate motif.

The path to such summaries is:

```bash
example_output/results/datasets/example_CTCF/nmf_scores/example_CTCF_left_f30_c3.txt
```

## FAQ ##

### Where can I get a reference genome and chromosome sizes? ###

```
## Download human genome (version hg38) sequence:
wget -c https://hgdownload.soe.ucsc.edu/goldenPath/hg38/bigZips/hg38.fa.gz -O - | gunzip > data/hg38.fa

## Download chromosome sizes of human genome (version hg38):
wget https://hgdownload.soe.ucsc.edu/goldenPath/hg38/bigZips/hg38.chrom.sizes -O data/hg38.chrom.sizes
```

### How can I re--build the publicly available container? ###

In this repository you can find a `Dockerfile` that was used to build the container and all requirements.
- System requirements: `requirements-sys.txt`
- R dependencies: `requirements.R`
- Python dependencies: `requirements.txt`

Note in the `Dockerfile` that we use a specific version of `Ghostscript 9.25`.

## Contact ##

In case you have any questions related to this pipeline, please contact the following persons:

| Name                                                      | E-mail                          |
| ----------------------------------------------------------|---------------------------------|
| [Ieva Rauluseviciute](https://bitbucket.org/ievara/)      | ieva.rauluseviciute@ncmm.uio.no |
| [Anthony Mathelier](https://mathelierlab.com/)            | anthony.mathelier@ncmm.uio.no   |
