##-------##
## About ##
##-------##

## Author: Ieva Rauluseviciute
## Affiliation: NCMM, UiO, Oslo, Norway

##----------------------##
## Libraries, functions ##
##----------------------##

## Import functions:
import os
import sys
import itertools
import time
import glob
from pathlib import Path
import subprocess

## Config file:
configfile: "config.yaml"

## Record start time:
start = time.time()

##-----------##
## Container ##
##-----------##

container: config["container"]
print("\n; Available container:\n %s" % config["container"])

##----------------------------------------------------------------##
## How to run the pipeline?                                       ##
## snakemake --cores 1                                            ##
##----------------------------------------------------------------##
## Using COBIND Docker container:                                 ##
## snakemake --use-singularity --singularity-args '\-e' --cores 1 ##
##----------------------------------------------------------------##

##-------------##
## Directories ##
##-------------##

MAIN_DIR_ABS = os.getcwd()
RESULTS_DIR  = os.path.join(config["title"], "results")
DATA_DIR     = config["DATA_DIRECTORY"]
RUN_TIME_DIR = config["title"]
REPORTS_DIR  = os.path.join(config["title"], "reports")

##-----------##
## Variables ##
##-----------##

## Variables from the config to write in the run_time text file:
config_title          = config["title"]
gini_threshold        = config["GINI_THRESHOLD"]
kim_park_threshold    = config["KIM_PARK_THRESHOLD"]
config_data           = config["DATA_DIRECTORY"]
config_flank          = config["TFBS_FLANK"]
config_components     = config["NB_COMPONENTS"]
config_groups         = config["DATASET_GROUPING"]


##---------------------------------##
## Compiling the C code, if needed ##
##---------------------------------##

if not os.path.exists(os.path.join(config["BIN"], "clustering", "matrix-clustering_stand-alone", "compare-matrices-quick", "compare-matrices-quick")):
    compile_cmd = ['make']

    try:
        os.chdir(os.path.join(config["BIN"], "clustering", "matrix-clustering_stand-alone", "compare-matrices-quick"))
        result = subprocess.run(compile_cmd, check=True, capture_output=True, text=True)
        os.chdir(MAIN_DIR_ABS)
        print("Compilation successful.")
    except subprocess.CalledProcessError as e:
        print("Compilation failed:")
        print(e.stderr)

##---------------##
## Info messages ##
##---------------##

## When pipeline is executed succesfully:
onsuccess:
    success_runtime = round((time.time() - start)/60,3)
    print("; Running time in minutes:\n %s" % success_runtime)
    f = open("{dir}/run_time.txt".format(dir = RUN_TIME_DIR), "w+")
    print("; Running time in minutes:\n %s" % success_runtime, file = f)
    print("\n; Analysis title:\n %s" % config_title, file = f)
    print("\n; Used Gini threshold:\n %s" % gini_threshold, file = f)
    print("\n; Used Kim&Park threshold:\n %s" % kim_park_threshold, file = f)
    print("\n; Used data:\n %s" % config_data, file = f)
    print("\n; Flank length:\n %s" % config_flank, file = f)
    print("\n; Components:\n %s" % config_components, file = f)
    print("\n; Grouping file used:\n %s" % config_groups, file = f)

    print("\n##---------------------------##\n; Workflow finished, no errors!\n##---------------------------##\n")

## When error occurs:
onerror:
    print("\n##-----------------##\n; An error occurred!\n##-----------------##\n")
    print("; Running time in minutes:\n %s\n" % round((time.time() - start)/60,1))

## When starting the run:
onstart:
    print("\n##-----------------------------------------##\n; Reading input and firing up the analysis...\n##-----------------------------------------##\n")
    print("\n; Analysis title:\n %s" % config_title)
    print("\n; Used Gini threshold:\n %s" % gini_threshold)
    print("\n; Used Kim&Park threshold:\n %s" % kim_park_threshold)
    print("\n; Used data:\n %s" % config_data)
    print("\n; Flank length:\n %s" % config_flank)
    print("\n; Components:\n %s" % config_components)
    print("\n; Grouping file used:\n %s" % config_groups)

##-------##
## Input ##
##-------##

### Defining input files:
def pair_name_to_infiles():
    bed_path = list(Path(DATA_DIR).glob('**/*.bed'))
    fasta_path = list(Path(DATA_DIR).glob('**/*.fa'))
    
    if bed_path and fasta_path:
        raise ValueError("; Both BED and FASTA files found in directory. Only one type is allowed.")
    
    infiles_dict = {}
    file_type = None
    if bed_path:
        file_type = "BED"
        for f in bed_path:
            file = f.name.replace('.bed', '')
            infiles_dict[file] = str(f)
        print("; Found BED files.")
    elif fasta_path:
        file_type = "FASTA"
        for f in fasta_path:
            file = f.name.replace('.fa', '')
            infiles_dict[file] = str(f)
        print("; Found FASTA files.")
    else:
        raise IOError("; No BED or FASTA files found in directory.")
    
    return file_type, infiles_dict

file_type, files_to_analyse = pair_name_to_infiles()

##--------------------------##
## Checking the input files ##
##--------------------------##

problematic_files = []

for file_name, file_path in files_to_analyse.items():
    ## Checking the lenght of the basenames of the files:
    basename_length = len(file_name)
    if basename_length > 65:
        file_and_name_length = [file_name, basename_length]
        problematic_files.append(file_and_name_length)
        print("\n; ERROR! A filename ", file_name, " is too long (longer than 65 characters).\n")
    ## Cheking if there are any dots in the file names:
    if '.' in file_name:
        problematic_files.append(file_name)
        print("\n; ERROR! A filename ", file_name, " contains a dot.\n")
    ## Checking if regions are of the same length:
    with open(file_path, 'r') as input_file:
        unique_region_sizes = set()
        fasta_anchor_check = set()
        for line in input_file.readlines():
            if file_type == "BED":
                columns = line.strip().split("\t")
                region_size = int(columns[2]) - int(columns[1])
                unique_region_sizes.add(region_size)
            else:
                if not line.startswith(">"):
                    region_size = len(line.strip())
                    anchor_length = (region_size - config_flank * 2)
                    fasta_anchor_check.add(anchor_length)
                    unique_region_sizes.add(region_size)
        nb_unique_sizes = len(unique_region_sizes)
        if region_size < 2:
            print("\n; WARNING! Input regions are less than 2 bp long for ", file_name, " file.\n")

    if nb_unique_sizes > 1:
        problematic_files.append(file_name)
        print("\n; ERROR! Regions in a file ", file_name, " are not of the same length.\n")
    elif any(item < 1 for item in fasta_anchor_check):
        problematic_files.append(file_name)
        print("\n; ERROR! Sequences in a file ", file_name, " are too short to contain both flanks and an anchor.\n")

if len(problematic_files) > 0:
    raise IOError("\n##-------------ERROR!------------##\n; Fix problems above and try again.\n##-------------------------------##\n")

##--------------##
## Output files ##
##--------------##

### Preprocessing:
FILTERED_BED                = os.path.join(RESULTS_DIR, "datasets", "{file}", "BED", "{file}.bed.oriented")
EXTENDED_BED_FULL           = os.path.join(RESULTS_DIR, "datasets", "{file}", "BED", "{file}.bed.oriented.full")
SUBTRACTED_BED_L            = os.path.join(RESULTS_DIR, "datasets", "{file}", "BED", "{file}.bed.oriented.{flank}l0r.subtd")
SUBTRACTED_BED_R            = os.path.join(RESULTS_DIR, "datasets", "{file}", "BED", "{file}.bed.oriented.0l{flank}r.subtd")

FASTA_L                     = os.path.join(RESULTS_DIR, "datasets", "{file}", "fasta", "{file}.bed.oriented.{flank}l0r.subtd.fa")
FASTA_R                     = os.path.join(RESULTS_DIR, "datasets", "{file}", "fasta", "{file}.bed.oriented.0l{flank}r.subtd.fa")
FASTA_CORE                  = os.path.join(RESULTS_DIR, "datasets", "{file}", "fasta", "{file}.bed.oriented.fa")
FASTA_FULL                  = os.path.join(RESULTS_DIR, "datasets", "{file}", "fasta", "{file}.bed.full.fa")

### NMF:
NMF_SUMMARY_L               = os.path.join(RESULTS_DIR, "datasets", "{file}", "nmf_scores", "{file}_left_f{flank}_c{nb_components}.txt")
NMF_SUMMARY_R               = os.path.join(RESULTS_DIR, "datasets", "{file}", "nmf_scores", "{file}_right_f{flank}_c{nb_components}.txt")

### Clustering:
CLUST_TABLE_LOG             =  os.path.join(RESULTS_DIR, "tf_families_tables", "tf_families_tables_log.txt")
CLUST_TABLE_CORE            = os.path.join(RESULTS_DIR, "tf_families_tables", "{family}_core.tab")
CLUST_TABLE_COBINDER        = os.path.join(RESULTS_DIR, "tf_families_tables", "{family}_cobinder.tab")

CLUSTERING_RESULTS_CORE     = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "core", "{family}_core_tables", "clusters.tab")
CLUSTERING_RESULTS_COBINDER = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "cobinder", "{family}_cobinder_tables", "clusters.tab")
ALIGNMENT_TABLE_CORE        = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "core", "{family}_core_tables", "alignment_table.tab")
ALIGNMENT_TABLE_COBINDER    = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "cobinder", "{family}_cobinder_tables", "alignment_table.tab")

### Spacings:
SPACINGS_SUMMARY            = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "spacings", "spacings.txt")
SPACING_HEATMAP             = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "spacings", "TF_cobinder_spacer_plot_{family}.pdf")
# SPACING_BOXPLOT             = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "spacings", "TF_cobinder_spacer_boxplot_{family}.pdf")

### Regions:
FULL_COBINDER_REGION        = os.path.join(RESULTS_DIR, "cobinder_regions", "BED", "full_regions", "with_cobinder", "with_cobinder_full_regions_{family}.bed")
FULL_NO_COBINDER_REGION     = os.path.join(RESULTS_DIR, "cobinder_regions", "BED", "full_regions", "no_cobinder", "no_cobinder_full_regions_{family}.bed")
FULL_COBINDER_REGION_FASTA        = os.path.join(RESULTS_DIR, "cobinder_regions", "FASTA", "full_regions", "with_cobinder", "with_cobinder_full_regions_{family}.fa")
FULL_NO_COBINDER_REGION_FASTA     = os.path.join(RESULTS_DIR, "cobinder_regions", "FASTA", "full_regions", "no_cobinder", "no_cobinder_full_regions_{family}.fa")

### GREAT analysis output:
GREAT_OUTPUT                =  os.path.join(RESULTS_DIR, "GREAT_analysis", "{family}", "full_region", "with_cobinder", "gene_tss_distances_table.tsv")

## Markdown report:
REPORT_HTML                 = os.path.join(REPORTS_DIR, "{family}", "COBIND_analysis_report_{family}.html")

## Final summary:
COBINDER_SUMMARY            = os.path.join(RESULTS_DIR, "analysis_overall_summary", "cobinder_distribution_summary_table.txt")

##----------##
## Rule all ##
##----------##

rule all:
    """
    Defining a final output, which will request all the rules to be run.
    """
    input:
        COBINDER_SUMMARY

##-------##
## Rules ##
##-------##

#######################
## 1. Pre-processing ##
#######################

if file_type == "BED" :

    rule generateFLANKSandANCHORS:
        """
        1.1. Flanking regions are generated. Based on the input file, the anchor regions are also oriented.
        """
        input:
            lambda wildcards: files_to_analyse[wildcards.file]
        output:
            SUBTRACTED_BED_L, \
            SUBTRACTED_BED_R
        message:
            "; Generating flanks for {wildcards.file}."
        params:
            bin         = os.path.join(config["BIN"], "preprocessing"), \
            output_dir  = os.path.join(RESULTS_DIR, "datasets", "{file}", "BED"), \
            cpp_script  = os.path.join(config["BIN"], "preprocessing", "Distance_strings.cpp"), \
            chrom_sizes = config["CHROM_SIZES"], \
            genome      = config["GENOME_FA"], \
            flank_size  = config["TFBS_FLANK"], \

            bed_fa      = os.path.join(RESULTS_DIR, "datasets", "{file}", "BED", "{file}.bed"), \
            bed_or      = os.path.join(RESULTS_DIR, "datasets", "{file}", "BED", "{file}.bed.or")
        shell:
            """
            n_columns=$(awk -F' ' '{{print NF; exit}}' {input}) ;

            if [ $n_columns == 6 ]
            then
                echo '; Six column BED file. Generating flanks.'

                Rscript {params.bin}/generate_flanks.R \
                -i {input} \
                -f {params.flank_size} \
                -o {params.output_dir} \
                -c {params.chrom_sizes}

            elif [ $n_columns == 3 ]
            then
                echo '; Three column BED file. Orienting and generating flanks.'

                bedtools getfasta -fi {params.genome} -bed {input} -tab -bedOut > {params.bed_fa}

                Rscript {params.bin}/orient_BED.R \
                -i {params.bed_fa} \
                -o {params.output_dir} \
                -c {params.cpp_script} ;

                Rscript {params.bin}/generate_flanks.R \
                -i {params.bed_or} \
                -f {params.flank_size} \
                -o {params.output_dir} \
                -c {params.chrom_sizes} ;

                rm {params.bed_fa}
                rm {params.bed_or}
            else
                echo '; Input BED does not have three or six columns. Check your input file {input}.'
            fi
            """

    rule bed2fasta:
        """
        1.2. Flanking regions are converted to FASTA.
        """
        input:
            subtracted_left = SUBTRACTED_BED_L, \
            subtracted_right = SUBTRACTED_BED_R
        output:
            fasta_left = FASTA_L, \
            fasta_right = FASTA_R
        message:
            "; Converting flanks to fasta for {wildcards.file}."
        params:
            genome        = config["GENOME_FA"], \
            oriented_bed  = FILTERED_BED, \
            extended_full = EXTENDED_BED_FULL, \
            fasta_core    = FASTA_CORE, \
            fasta_full    = FASTA_FULL
        shell:
            """
            bedtools getfasta -fi {params.genome} -bed {input.subtracted_left} -s -fo {output.fasta_left} ;
            bedtools getfasta -fi {params.genome} -bed {input.subtracted_right} -s -fo {output.fasta_right} ;

            bedtools getfasta -fi {params.genome} -bed {params.oriented_bed} -s -fo {params.fasta_core} ;
            bedtools getfasta -fi {params.genome} -bed {params.extended_full} -s -fo {params.fasta_full} ;
            """

else:

    rule generateFASTAflanksANDanchors:
        """
        1.3. Anchor and flanking regions are generated from the input fasta regions.
        """
        input:
            lambda wildcards: files_to_analyse[wildcards.file]
        output:
            fasta_left = FASTA_L, \
            fasta_right = FASTA_R
        message:
            "; Extracting anchor and flanking regions for {wildcards.file}."
        params:
            bin        = os.path.join(config["BIN"], "preprocessing"), \
            flank_size = config["TFBS_FLANK"], \
            output_dir = os.path.join(RESULTS_DIR, "datasets", "{file}", "fasta")
        shell:
            """
            Rscript {params.bin}/generate_fasta_flanks.R \
            -i {input} \
            -f {params.flank_size} \
            -o {params.output_dir}
            """

############
## 2. NMF ##
############

rule runNMF:
    """
    2.1. Running non-negative matrix factorization on flanking regions to discover co-binder motifs. If any significant motifs discovered, they are trimmed and anchor motifs recovered using a subset of sequences.
    """
    input:
        fasta_left = FASTA_L, \
        fasta_right = FASTA_R
    output:
        NMF_SUMMARY_L, \
        NMF_SUMMARY_R
    message:
        "; Running NMF on flanking regions for {wildcards.file}."
    params:
        bin                = os.path.join(config["BIN"], "NMF"), \
        output_folder      = os.path.join(RESULTS_DIR, "nmf_motifs"), \
        score_folder       = os.path.join(RESULTS_DIR, "datasets", "{file}", "nmf_scores"), \
        nb_components      = config["NB_COMPONENTS"], \
        exp_name_left      = "{file}_left", \
        exp_name_right     = "{file}_right", \
        gini_threshold     = config["GINI_THRESHOLD"], \
        kim_park_threshold = config["KIM_PARK_THRESHOLD"], \
        fasta_core         = FASTA_CORE
    shell:
        """
        python {params.bin}/apply_NMF_trim.py \
            {input.fasta_left} \
            {params.output_folder} \
            {params.score_folder} \
            {params.exp_name_left} \
            {wildcards.nb_components} \
            {params.gini_threshold} \
            {params.kim_park_threshold} \
            {params.fasta_core} ;

        python {params.bin}/apply_NMF_trim.py \
            {input.fasta_right} \
            {params.output_folder} \
            {params.score_folder} \
            {params.exp_name_right} \
            {wildcards.nb_components} \
            {params.gini_threshold} \
            {params.kim_park_threshold} \
            {params.fasta_core} ;
        """

###################
## 3. Clustering ##
###################

checkpoint prepareCLUSTERINGtable:
    """
    3.1. Preparing tables with discovered co-binders and anchors files, which is then used as an input for clustering algorithm.
    """
    input:
        expand(NMF_SUMMARY_L, file = files_to_analyse.keys(), flank = config["TFBS_FLANK"], nb_components = config["NB_COMPONENTS"]), \
        expand(NMF_SUMMARY_R, file = files_to_analyse.keys(), flank = config["TFBS_FLANK"], nb_components = config["NB_COMPONENTS"])
    output:
        directory(os.path.join(RESULTS_DIR, "tf_families_tables"))
    message:
        "; Creating group motifs tables."
    params:
        bin = os.path.join(config["BIN"], "clustering"), \
        output_folder = os.path.join(RESULTS_DIR, "tf_families_tables"), \
        tf_families = config["DATASET_GROUPING"], \
        nmf_motifs = os.path.join(RESULTS_DIR, "nmf_motifs")
    shell:
        """
        python {params.bin}/make_clust_tables.py \
            {params.tf_families} \
            {params.nmf_motifs} \
            {params.output_folder} ;
        """

rule clusterMOTIFS:
    """
    3.2. Clustering co-binders and anchor motifs independently. If there is only one motif, the output is created directly.
    """
    input:
        table_core     = CLUST_TABLE_CORE, \
        table_cobinder = CLUST_TABLE_COBINDER
    output:
        CLUSTERING_RESULTS_CORE, \
        CLUSTERING_RESULTS_COBINDER, \
        ALIGNMENT_TABLE_CORE, \
        ALIGNMENT_TABLE_COBINDER
    message:
        "; Clustering core and cobinder motifs for {wildcards.family}."
    params:
        bin                    = os.path.join(config["BIN"], "clustering"), \
        output_folder_core     = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "core", "{family}_core"), \
        output_folder_cobinder = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "cobinder", "{family}_cobinder"), \
        cor                    = config["COR"], \
        ncor                   = config["NCOR"]
    shell:
        """
        n_lines=$(wc -l {input.table_cobinder} | awk '{{ print $1 }}')

        if [ $n_lines == 1 ]
        then
            echo '; Number of motifs is 1. Transfering one motif.'

            python {params.bin}/cluster_one_motif.py \
                {input.table_cobinder} {params.output_folder_cobinder} ;

            python {params.bin}/cluster_one_motif.py \
                {input.table_core} {params.output_folder_core}
        else
            echo '; Number of motifs is more than 1. Clustering.'

            Rscript {params.bin}/matrix-clustering_stand-alone/matrix-clustering.R \
            -i {input.table_core} \
            -m cor --cor_th 0.75 --Ncor 0.55 \
            --number_of_workers 1 \
            --minimal_output TRUE \
            -o {params.output_folder_core} ;

            Rscript {params.bin}/matrix-clustering_stand-alone/matrix-clustering.R \
            -i {input.table_cobinder} \
            -m cor --cor_th {params.cor} --Ncor {params.ncor} \
            --number_of_workers 1 \
            --minimal_output TRUE \
            -o {params.output_folder_cobinder}

        fi
        """
        
#################
## 4. Spacings ##
#################

rule computeSPACINGS:
    """
    4.1. Computing the spacings between co-binder and anchor motifs.
    """
    input:
        CLUSTERING_RESULTS_CORE, \
        CLUSTERING_RESULTS_COBINDER, \
        ALIGNMENT_TABLE_CORE, \
        ALIGNMENT_TABLE_COBINDER
    output:
        SPACINGS_SUMMARY
    message:
        "; Computing the spacers for {wildcards.family}."
    params:
        bin                        = os.path.join(config["BIN"], "spacings"), \
        clustering_bin             = os.path.join(config["BIN"], "clustering"), \
        core_clustering_folder     = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "core"), \
        cobinder_clustering_folder = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "cobinder"), \
        output_folder              = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "spacings")
    shell:
        """
        export COBIND_CLUSTERING={params.clustering_bin}/matrix-clustering_stand-alone ;

        python {params.bin}/compute_spacers.py \
            {params.core_clustering_folder} \
            {params.cobinder_clustering_folder} \
            {wildcards.family} \
            {params.output_folder} ;
        """

rule spacingPLOTS:
    """
    4.2. Drawing a spacings summary plot.
    """
    input:
        SPACINGS_SUMMARY
    output:
        SPACING_HEATMAP
    message:
        "; Summarizing spacers for {wildcards.family}."
    params:
        bin          = os.path.join(config["BIN"], "spacings"), \
        spacings_dir =  os.path.join(RESULTS_DIR, "clustering_results", "{family}", "spacings"), \
        title        = config["title"]
    shell:
        """
        Rscript {params.bin}/draw_spacers_heatmap.R \
        -i {params.spacings_dir} \
        -t {params.title} \
        -f {wildcards.family} ;
        """

        # Rscript {params.bin}/draw_spacers_boxplot_dataset_specific.R \
        # -i {params.spacings_dir} \
        # -t {params.title} \
        # -f {wildcards.family} ;
################
## 5. Regions ##
################

if file_type == "BED":

    rule extractREGIONS:
        """
        5.1. Extracting genomic regions: anchors-only with and without co-binders, full regions (anchor + flanks) with and without co-binders.
        """
        input:
            SPACING_HEATMAP
        output:
            FULL_COBINDER_REGION, \
            FULL_NO_COBINDER_REGION
        message:
            "Extracting full regions with and without co-binders for {wildcards.family}."
        params:
            bin          = os.path.join(config["BIN"], "regions"), \
            spacings_dir =  os.path.join(RESULTS_DIR, "clustering_results", "{family}", "spacings"), \
            title        = config["title"], \
            input_type   = file_type
        shell:
            """
            Rscript {params.bin}/extract_regions.R \
            -i {params.spacings_dir} \
            -t {params.title} \
            -f {wildcards.family} \
            -g {params.input_type} ;
            """

else:

    rule extractREGIONS:
        """
        5.2. Extracting genomic regions: anchors-only with and without co-binders, full regions (anchor + flanks) with and without co-binders.
        """
        input:
            SPACING_HEATMAP
        output:
            FULL_COBINDER_REGION_FASTA, \
            FULL_NO_COBINDER_REGION_FASTA
        message:
            "Extracting full regions with and without co-binders for {wildcards.family}."
        params:
            bin          = os.path.join(config["BIN"], "regions"), \
            spacings_dir =  os.path.join(RESULTS_DIR, "clustering_results", "{family}", "spacings"), \
            title        = config["title"], \
            input_type   = file_type
        shell:
            """
            Rscript {params.bin}/extract_centered_regions.R \
            -i {params.spacings_dir} \
            -t {params.title} \
            -f {wildcards.family} \
            -g {params.input_type} ;
            """

## Running GREAT only if it is specified:
if ((config['GREAT_GENOME'] == "hg38") | \
    (config['GREAT_GENOME'] == "hg19") | \
    (config['GREAT_GENOME'] == "mm10") | \
    (config['GREAT_GENOME'] == "mm9")) \
    and file_type == "BED":

    rule runGREAT:
        """
        5.3. If genome version is specified, GREAT regios enrichment is run on regions with and without co-binders.
        """
        input:
            FULL_COBINDER_REGION, \
            FULL_NO_COBINDER_REGION
        output:
            GREAT_OUTPUT
        message:
            "; Running GREAT analysis for regions with and without co-binder for {wildcards.family}."
        params:
            bin                         = os.path.join(config["BIN"], "regions"), \
            genome_version              = config["GREAT_GENOME"], \
            full_with_cobinder          = FULL_COBINDER_REGION, \
            full_no_cobinder            = FULL_NO_COBINDER_REGION ,\
            output_full_with_cobinder   =  os.path.join(RESULTS_DIR, "GREAT_analysis", "{family}", "full_region", "with_cobinder"), \
            output_full_no_cobinder     =  os.path.join(RESULTS_DIR, "GREAT_analysis", "{family}", "full_region", "no_cobinder"), \
            output_cobinder_specific    =  os.path.join(RESULTS_DIR, "GREAT_analysis", "{family}", "cobinder_specific"), \
            full_specific_with_cobinder = os.path.join(RESULTS_DIR, "cobinder_regions", "BED", "full_regions", "with_cobinder_motif_specific", "{family}"), \
            full_specific_no_cobinder   = os.path.join(RESULTS_DIR, "cobinder_regions", "BED", "full_regions", "no_cobinder_motif_specific", "{family}")
        shell:
            """
            mkdir -p {params.output_full_with_cobinder} ;
            mkdir -p {params.output_full_no_cobinder} ;
    
            Rscript {params.bin}/run_GREAT.R \
                -p {params.full_with_cobinder} \
                -o {params.output_full_with_cobinder} \
                -g {params.genome_version} ;
    
            n_regions_no_cobinder=$(wc -l {params.full_no_cobinder} | awk '{{ print $1 }}') ;
            if (( $n_regions_no_cobinder <= 500000 )) ;
            then
                Rscript {params.bin}/run_GREAT.R \
                    -p {params.full_no_cobinder} \
                    -o {params.output_full_no_cobinder} \
                    -g {params.genome_version}
            fi;
    
            for file in {params.full_specific_with_cobinder}/full_regions_with_cobinder_*.bed ;
                do
                    filename=$(basename -- "$file") ;
                    cobinder_id="${{filename%.bed}}" ;
                    cobinder_id="${{cobinder_id//"full_regions_with_cobinder_"/}}" ;
                    with_output_folder={params.output_cobinder_specific}/cobinder_"$cobinder_id"/with_cobinder ;
                    mkdir -p $with_output_folder ;
                    no_output_folder={params.output_cobinder_specific}/cobinder_"$cobinder_id"/no_cobinder ;
                    mkdir -p $no_output_folder ;
                    echo "; Running GREAT analysis for cobinder $cobinder_id" ;
    
                    regions_with_cobinder={params.full_specific_with_cobinder}/full_regions_with_cobinder_"$cobinder_id".bed ;
                    regions_no_cobinder={params.full_specific_no_cobinder}/full_regions_no_cobinder_"$cobinder_id".bed ;
    
                    Rscript {params.bin}/run_GREAT.R \
                        -p "$regions_with_cobinder" \
                        -o "$with_output_folder" \
                        -g {params.genome_version} ;
    
                    n_spec_regions_no_cobinder=$(wc -l "$regions_no_cobinder" | awk '{{ print $1 }}') ;
                    if (( $n_spec_regions_no_cobinder <= 500000 )) ;
                    then
                        Rscript {params.bin}/run_GREAT.R \
                            -p "$regions_no_cobinder" \
                            -o "$no_output_folder" \
                            -g {params.genome_version}
                    fi ;
            done ;
            """
else:
    print("; Skipping GREAT. If you want to run region enrichment with GREAT, used BED input and define genome assembly in config.yaml")

###############
## 6. Report ##
###############

## Defining input:
def report_input(wildcards):
    if ((config['GREAT_GENOME'] == "hg38") | (config['GREAT_GENOME'] == "hg19") | (config['GREAT_GENOME'] == "mm10") | (config['GREAT_GENOME'] == "mm9") and file_type == "BED"):
        return GREAT_OUTPUT
    elif file_type == "BED":
        return FULL_COBINDER_REGION
    else:
        return FULL_COBINDER_REGION_FASTA

rule generateREPORT:
    """
    6.1. Summarizing the results into a HTML report.
    """
    input:
        report_input
    output:
        REPORT_HTML
    message:
        "; Generating analysis report for {wildcards.family}."
    params:
        bin           = os.path.join(config["BIN"], "report"), \
        title         = config["title"]
    shell:
        """
        echo "Rscript {params.bin}/generate_report.R \
        -f {wildcards.family} \
        -a {params.title} \
        -r {params.bin}/cobind_report_template.Rmd" > \
        {params.title}/reports/{wildcards.family}/command.txt ;

        Rscript {params.bin}/generate_report.R \
        -f {wildcards.family} \
        -a {params.title} \
        -r {params.bin}/cobind_report_template.Rmd
        """

## Function that defines new wildcard "family", so the pipeline uses the checkpoint to run uninterupted.
def aggregate_reports(wildcards):
    checkpoint_output = checkpoints.prepareCLUSTERINGtable.get(**wildcards).output[0]
    file_names = expand(REPORT_HTML,
                        family = glob_wildcards(os.path.join(checkpoint_output, "{family}_core.tab")).family)
    return file_names

################
## 6. Summary ##
################

rule distributionPLOTS:
    """
    6.1. Summarizing the numbers of discovered co-binders.
    """
    input:
        aggregate_reports
    output:
        COBINDER_SUMMARY
    params:
        bin                    = os.path.join(config["BIN"], "summary"), \
        clustering_dir         =  os.path.join(RESULTS_DIR, "clustering_results"), \
        title                  = config["title"], \
        tf_families_tables_log = CLUST_TABLE_LOG
    shell:
        '''
        n_lines=$(wc -l {params.tf_families_tables_log} | awk '{{ print $1 }}')

        if [ $n_lines == 0 ]
        then
            echo '; No co-binders were found during analysis.'
            echo 'No co-binders were found during analysis.' > {output} ;
        else
            echo '; Summarizing co-binder analysis.'
            Rscript {params.bin}/cobinder_overall_distribution.R \
            -i {params.clustering_dir} \
            -t {params.title} ;
        fi
        '''

##------------------##
## End of Snakefile ##
##------------------##